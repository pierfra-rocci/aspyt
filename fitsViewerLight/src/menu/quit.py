import sys

class quit():
    """Class managing the Exit button of the menu"""

    def __init__(self):
        """Exit the software"""

        sys.exit()