##########
# IMPORT #
##########

from PySide2 import QtWidgets
from PySide2.QtWidgets import QCheckBox, QSizePolicy, QHBoxLayout, QComboBox
from astropy.io import fits
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import mplcyberpunk
import matplotlib.style
#Matplot style
#['seaborn-whitegrid', 'seaborn-poster', 'classic', 'seaborn-darkgrid', 'seaborn-ticks', 'seaborn-dark-palette', 'bmh', 'seaborn-notebook', 'fivethirtyeight', 'seaborn-talk', 'ggplot', 'seaborn-dark', 'seaborn-muted', 'seaborn-white', 'seaborn-pastel',
#  'fast', 'Solarize_Light2', 'seaborn-paper', 'seaborn-colorblind', '_classic_test', 'seaborn', 'seaborn-bright', 'grayscale', 'seaborn-deep', 'dark_background']

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from matplotlib.widgets import Cursor, RectangleSelector
from numpy import arange, amin, amax, average
from specutils import Spectrum1D
import astropy.units as u
from utils.imageEditor import ImageEditor
import tempfile
import re
import os
import shutil


class SpectrumManager:
    """
    Class managing FITS/FIT files containing only a spectrum

    Attributes
    ----------
    headerEntryCount int
        counter of the number of fields of the header of the open file
    openedFile str
        path of the open file
    show_second_axis boolean
        boolean for show/hide second axis ax2
    ax2 object
        second axis of matplotlib plot
    canvas QTCanvas
        canvas for plot

    '''

    Methods
    -------
    loadFitsFile(filePath, ui)
        Retrieves the data present in the header and displays the spectrum in the software
    onclick(event)
        Updates the cursor position on the image when clicking
    editFitsFileHeader(datalist,ui)
        Saves the changes made to the header in the open fits file
    def set_cbk_style()
        Enable/Disable the cyberpunk theme
    show_ax2()
        Show/Hide the second axis of the plot 
    """

    headerEntryCount = 0
    openedFile = ""
    show_second_axis = True
    ax2 = ""
    canvas = ""


    @staticmethod
    def loadFitsFile(filePath, ui):
        """
        Retrieves the data present in the header and displays the spectrum in the software
        
        Parameters
        ----------
        filePath : str
            path to the fits file
        ui : MyWindow
            Software window where the spectrum will be displayed
        """

        SpectrumManager.openedFile = filePath

        # print path file in console
        print(SpectrumManager.openedFile)

        # get VBoxLayout for show spectrum
        embedMPL = ui.findChildren(QtWidgets.QVBoxLayout, 'embedMPL')[0]
        ui.findChildren(QtWidgets.QTabWidget, 'left')[0].setTabText(0, "Spectre")

        for i in reversed(range(embedMPL.count())):
            embedMPL.itemAt(i).widget().setParent(None)
        image_data = fits.getdata(filePath)

        hdul = fits.open(filePath)
        data = hdul[0].data # spectrum data
        header = hdul[0].header # header

        maxPix = header['NAXIS1'] # maximum pixel
        refPix = header['CRPIX1']-1 # reference pixel

        # convert pixels to wavelength 
        cdelt = header['CDELT1']
        lambda1 = header['CRVAL1'] - cdelt * refPix
        lambda2 = header['CRVAL1'] + cdelt * (maxPix - refPix)

        l = arange(lambda1, lambda2, cdelt) * u.AA # lambda
        i = data * u.Jy # flux

        spec1d = Spectrum1D(spectral_axis=l, flux=i)

        #-----------------------------------#
        spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + str(header['EXPTIME']) +  's. - ' + header['BSS_INST'] + ' - ' + header['OBSERVER']

        fig, ax1 = plt.subplots(figsize=(16, 9))


        label1 = header['OBJNAME'] + ' - ' + header['DATE-OBS']
        ax1.plot(spec1d.spectral_axis, spec1d.flux, linewidth=0.8, label = label1)
        #label2 = "spec2"
        #ax1.plot(spec1d.spectral_axis, spec1d.flux*2, color='green', linewidth=0.8, label = label2)
        #label3 = "spec3"
        #ax1.plot(spec1d.spectral_axis, spec1d.flux*3, color='green', linewidth=0.8, label = label3)


        ################################################################################
        #                              Plot design                                     #
        ################################################################################

        # ----- AX1 -----#
        # add Graph title
        ax1.set_title(spectrumTitle, loc='center', fontsize=10, fontweight=0)

        # add X axis label
        ax1.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'], fontdict=None, labelpad=None)

        # add Y axis label
        ax1.set_ylabel('Relative Flux', fontdict=None, labelpad=None)

        ax1.grid(alpha=0.8, linestyle='-.', linewidth=0.3, axis='both')
        legend_line = header['OBJNAME'] + ' - ' + header['DATE-OBS']
        #ax1.legend([label1, label2, label3], loc=('upper left'))
        ax1.legend([label1], loc=('upper left'))

        start_x, end_x = ax1.get_xlim()
        start_y, end_y = ax1.get_ylim()
        ax1.set(ylim=(start_y, end_y), xlim=(start_x, end_x))


        # ---- AX2 ------#
        ax2 = ax1.twinx()
        SpectrumManager.ax2 = ax2
        ax2.plot(spec1d.spectral_axis, spec1d.flux, alpha=0.3, linewidth = 0.5)
        #ax2.set_ylabel("---")
        #ax2.legend(["---"], loc=('upper right'))

        start_x, end_x = ax1.get_xlim()
        start_y, end_y = ax1.get_ylim()

        # set limit here for modify second spectrum ratio
        print(start_x, end_x, start_y, end_y)
        ax2.set(ylim=(start_y/4, end_y/4))

        if(SpectrumManager.show_second_axis):
            ax2.set_visible(True)
            print('ax2 visible')
        else:
            ax2.set_visible(False)
            print('x2 not visible')

        # add figure to canvas
        canvas = FigureCanvas(fig)
        SpectrumManager.canvas = canvas
        SpectrumManager.fig = fig
        embedMPL.addWidget(canvas)

        #Show matplotlib navigationBar
        embedMPL.addWidget(NavigationToolbar(canvas, canvas))

        
        ################################################################################
        #                 Layout modification for Spectrum Managing                    #
        ################################################################################

        #Print path file label in status bar
        pathFileValueLabel = ui.findChildren(QtWidgets.QLabel, 'path_file_value_label')[0]
        pathFileValueLabel.setText(SpectrumManager.openedFile)

        #Print minimum pixel value in status bar
        min_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'min_pixel_value_label')[0]
        min_pixel_value_label.setText(str(round(amin(data),4)))

        #Print average pixel value in status bar
        avg_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'avg_pixel_value_label')[0]
        avg_pixel_value_label.setText(str(round(average(data), 4)))

        #Print maximum pixel value in status bar
        max_pixel_value_label = ui.findChildren(QtWidgets.QLabel, 'max_pixel_value_label')[0]
        max_pixel_value_label.setText(str(round(amax(data),4)))

        # Activation "Update fits header" btn
        updatefits_btn = ui.findChildren(QtWidgets.QPushButton, 'updatefits_btn')[0]
        updatefits_btn.setEnabled(True)


        show_ax2_btn = ui.findChildren(QtWidgets.QCheckBox, 'second_axis_ckbox')[0]
        SpectrumManager.show_ax2_btn = show_ax2_btn

        change_theme_btn = ui.findChildren(QtWidgets.QCheckBox, 'cybpk_ckbox')[0]
        SpectrumManager.change_theme_btn = change_theme_btn


        cursor = Cursor(ax2, horizOn=True, vertOn = True, useblit=True, linewidth=0.3)

        def onclick(event):
            """
            Updates the cursor position on the spectrum when clicking
            and print in the status bas
        
            Parameters
            ----------
            event :
                Allows/get the cursor click
            """

            cursor.onmove(event)
            x1, y1 = event.xdata, event.ydata
            inv = ax1.transData.inverted()
            ax1x, ax1y = inv.transform((event.x, event.y))
            print(ax1x, ax1y)
            canvas.toolbar.set_message('Flux = {} / Wavelength = {}'.format(np.round(ax1x, 3), np.round(ax1y, 3)))

        def mouse_move(event):
            """
            Updates the cursor position on the spectrum when moving
            and print in the status bas
        
            Parameters
            ----------
            event :
                Allows/get the cursor moves
            """

            cursor.onmove(event)
            x1, y1 = event.xdata, event.ydata
            inv = ax1.transData.inverted()
            ax1x, ax1y = inv.transform((event.x, event.y))
            canvas.toolbar.set_message('Flux = {} / Wavelength = {}'.format(np.round(ax1x, 3), np.round(ax1y, 3)))


        # connect cursor to methods
        canvas.mpl_connect('button_press_event', onclick)
        canvas.mpl_connect('motion_notify_event', mouse_move)



        ################################################################################
        #                            FITS Header Managing                              #
        ################################################################################

        # retrieving the header and inserting it into the table for show
        table = ui.findChildren(QtWidgets.QTableWidget, 'tableWidget')[0]

        table.setRowCount(0)

        header = fits.getheader(filePath)
        i = 0
        for line in header:
            table.insertRow(table.rowCount())
            table.setItem(table.rowCount() - 1, 0, QtWidgets.QTableWidgetItem(line))
            table.setItem(table.rowCount() - 1, 1, QtWidgets.QTableWidgetItem(str(header[i])))
            i = i + 1
        SpectrumManager.headerEntryCount = i;

        menuOpened_file = ui.findChildren(QtWidgets.QMenu, 'menuOpened_file')[0]
        menuOpened_file.setTitle("Opened File : "+re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",SpectrumManager.openedFile).group(2))
        
        tabWidget = ui.findChildren(QtWidgets.QTabWidget, 'left')[0]
        tabWidget.setCurrentWidget(tabWidget.findChildren(QtWidgets.QWidget, 'tab')[0])
        

    def editFitsFileHeader(datalist,ui):
        """
        Saves the changes made to the header in the open fits file.
        
        Parameters
        ----------
        datalist : QTableWidget
            table containing all the header data.
        ui : MyWindow
            Window displaying the spectrum and header information. Will be updated after the changes made in the header.
        """

        fileName = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)",SpectrumManager.openedFile).group(2)
        fileExt = re.search(r"(/|\\)(\b[^\\/]*\b)(\.fits|\.fit)", SpectrumManager.openedFile).group(3)
        temp = tempfile.NamedTemporaryFile(mode="w", prefix=fileName+str("-"),suffix=fileExt,delete=False)
        temp.close()

        file = fits.open(SpectrumManager.openedFile, mode='update')
        for i in range(0, SpectrumManager.headerEntryCount):
            value = datalist.item(i, 1).text()  # retrieving the value of the list
            file[0].header[i] = type(file[0].header[i])(value)  # assigning the list value to the header
        file[0].writeto(temp.name, overwrite=True)  # writing the modified FITS file
        file.close()

        os.remove(SpectrumManager.openedFile)
        shutil.move(temp.name,SpectrumManager.openedFile)
        SpectrumManager.loadFitsFile(SpectrumManager.openedFile,ui)
        

    def set_cbk_style(self):
        """
        Change style of the plot.
        Enable/disable cyberpunk style
        """

        if SpectrumManager.change_theme_btn.isChecked():
            print('Cyberpunk style actived')
            plt.style.use("cyberpunk")
            SpectrumManager.loadFitsFile(SpectrumManager.openedFile, self.ui)
            mplcyberpunk.make_lines_glow()
            mplcyberpunk.add_underglow()
        else:
            print('Cyberpunk style desactived')
            mpl.rcParams.update(mpl.rcParamsDefault)
            plt.style.use('default')
            SpectrumManager.loadFitsFile(SpectrumManager.openedFile, self.ui)


    def show_ax2(self):
        """
        Enable/Disable ax2 in background on spectrum plot
        """

        if SpectrumManager.show_ax2_btn.isChecked():
            SpectrumManager.show_second_axis = True
            SpectrumManager.ax2.set_visible(True)
            SpectrumManager.canvas.draw()
        else:
            SpectrumManager.show_second_axis = False
            SpectrumManager.ax2.set_visible(False)
            SpectrumManager.canvas.draw()
        