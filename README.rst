==========
ASPyT
==========
Astronomy & Spectroscopy Python Toolbox

|python| |dependencies| |licence|

#SpecBok
============
SpecBok script parses astronomical Fits files for initiate a Spectrum Graph and generate matplotlib Spectrum or a web interactive file in HTML / JavaScript.

Installation - SpecBok
------------

Astropy : https://www.astropy.org/

Specutils https://specutils.readthedocs.io/en/stable/spectrum1d.html

Numpy https://numpy.org/

Matplotlib https://matplotlib.org/)

Bokeh - Visualization library https://bokeh.org/


(use requirements.txt for a quick install with pip)


Features - SpecBok
--------

- Generate a Matplotlib Graph
- Generate a web interactive graph file in HTML / JavaScript. 

Usage - SpecBok
--------

- For generate a Matplotlib Graph
``$ python3 specbok.py pathToFitsFile``

- For generatea web interactive graph in HTML/JS
``$ python3 specbok.py pathToFitsFile web``

- For generatea web interactive graph in HTML/JS with Balmer Lines
``$ python3 specbok.py pathToFitsFile web showBalmer``



#FitsViewerLight
============
Application in Python for show Fits Image or Spectrum in a plot (matplotlib).

Installation - FitsViewerLight
------------

Astropy : https://www.astropy.org/

Specutils https://specutils.readthedocs.io/en/stable/spectrum1d.html

Numpy https://numpy.org/

Matplotlib https://matplotlib.org/)

PySide2 https://wiki.qt.io/Qt_for_Python

(use requirements.txt for a quick install with pip)

Features - FitsViewerLight
--------

- Open a Fits Image
- Open a Spectrum File 
- Enable/Disbale Second Axis for Spectrum Plot
- Enable/Disbale Cyberpunk Style for Spectrum Plot
- Show FITS Header
- Modify Fits Header

Usage - FitsViewerLight
--------

- For Launch Application 
``$ cd src``
``$ python3 main.py``

- For open a FITS Image 
Drag & Drop the file on Application
Or open file with the menu "File > Open file" 

- For open a FITS Spectrum 
Drag & Drop the file on Application
Or open file with the menu "File > Open spectrum" 




#Notebooks List
============

- 3d_spec_timeseries : Show a 3D plot of a spectrum list with Specutils & Plotly (Surface3D). Specs are in spec3d_data folder

    Launch Notebook : |binder-spec3d|



.. |binder-spec3d| image:: https://mybinder.org/badge_logo.svg
    :target: https://mybinder.org/v2/gl/chronosastro%2Faspyt/master?filepath=notebooks%2F3d_spec_timeseries.ipynb


.. |python| image:: https://img.shields.io/badge/Python-3.7%203.8-green
    :alt: Python
    :scale: 100%
    :target: https://www.python.org/

.. |dependencies| image:: https://img.shields.io/badge/dependencies-astropy-orange
    :alt: Dependencies
    :scale: 100%
    :target: https://www.astropy.org/

.. |licence| image:: https://img.shields.io/badge/licence-GPL%203.0-orange
    :alt: Licence gpl-v3.0
    :scale: 100%
    :target: https://www.gnu.org/licenses/gpl-3.0.fr.html
