Notebooks List
============
|dependencies|



- 3d_spec_timeseries : Show a 3D plot of a spectrum list with Specutils & Plotly (Surface3D). Specs are in spec3d_data folder

    Launch Notebook : |binder-spec3d|

- python_astro_spectro : General usage examples of Python in astronomy and spectroscopy with astropy, specutils, matplotlib (...). 

    Notebook used for the post preparation : https://stellartrip.net/python-pour-lastronomie-et-la-spectroscopie-analyse-rapide-dun-spectre/

- workshop_shelyak (folder) : Workshop Python pour l'astro et la spectro (N°1)

.. |binder-spec3d| image:: https://mybinder.org/badge_logo.svg
    :target: https://mybinder.org/v2/gl/chronosastro%2Faspyt/master?filepath=notebooks%2F3d_spec_timeseries.ipynb


.. |python| image:: https://img.shields.io/badge/Python-3.7%203.8-green
    :alt: Python
    :scale: 100%
    :target: https://www.python.org/

.. |dependencies| image:: https://img.shields.io/badge/dependencies-astropy-orange
    :alt: Dependencies
    :scale: 100%
    :target: https://www.astropy.org/

.. |licence| image:: https://img.shields.io/badge/licence-GPL%203.0-orange
    :alt: Licence gpl-v3.0
    :scale: 100%
    :target: https://www.gnu.org/licenses/gpl-3.0.fr.html
