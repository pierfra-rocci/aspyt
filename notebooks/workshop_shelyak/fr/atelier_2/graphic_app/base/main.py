#for last version mac os
import os
os.environ["QT_MAC_WANTS_LAYER"] = "1"

#imports
import sys
from PySide2.QtWidgets import QApplication, QMainWindow,  QFileDialog
from PySide2.QtCore import QFile, Slot
from PySide2 import QtGui #add an icon ;)
from ui_mainwindow import Ui_MainWindow

import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from astropy.io import fits
import astropy.units as u
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.figure import Figure

import numpy as np


#For windows usage
#import PySide2
##dirname = os.path.dirname(PySide2.__file__)
#plugin_path = os.path.join(dirname, 'plugins', 'platforms')
#os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = plugin_path

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        #Get button reference and set a function on click
        opn_btn = self.ui.pushButton
        opn_btn.clicked.connect(lambda: self.open_fits_spec())


    #----------- Functions here -------#
    #Prepare a spec1d form a fits file
    def prepare_spec1d(self, fits_file_path):

        #open file
        f = fits.open(fits_file_path)
        
        #get data
        header = f[0].header
        data = f[0].data
        
        # Precise flux units
        flux = data * u.Jy
        
        #------ with WCS object -----#
        wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                    'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                    'CRPIX1': header['CRPIX1']})
        
        # Create a Spec1D object with specutils
        spec = Spectrum1D(flux=flux, wcs=wcs_data)
        
        return spec, header


    #Show a spec1d
    def show_spec1d(self, spec1d, header):
    
        # Figure init
        fig01, ax01 = plt.subplots(figsize=(16, 9))
        
       
        # Set data for plot
        ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
        
        # X axis title
        ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
        
        # Y axis title
        ax01.set_ylabel('Relative Flux')
        
        # Grid config
        ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
            
        # Legend
        legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
        ax01.legend([legend_value], loc=('best'))
        
        
        # Create title with header data
        spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
        ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
        
        # Png record
        #plt.savefig('spectrum.png')
        
        # Show plot
        plt.show()
        print('Plot generated.')
    

    #Open a file
    def open_fits_spec(self):
        print('Select a file')
        filename = QFileDialog.getOpenFileName(self)
        print(filename[0])
        sp, hd = self.prepare_spec1d(filename[0])
        self.show_spec1d(sp, hd)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('shelyak_logo.png'))
    window = MainWindow()
    window.show()

    sys.exit(app.exec_())