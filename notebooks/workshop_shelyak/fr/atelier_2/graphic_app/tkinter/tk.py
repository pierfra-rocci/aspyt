
#tkinter import
from tkinter import *

#Create a window
fen1 = Tk()

#create a label
tex1 =Label(fen1, text='Hello World !')

#add label to window
tex1.pack()

#create a button
bou1 = Button(fen1, text='Quit', command = fen1.destroy)

#add button to window
bou1.pack()

#start window loop
fen1.mainloop()