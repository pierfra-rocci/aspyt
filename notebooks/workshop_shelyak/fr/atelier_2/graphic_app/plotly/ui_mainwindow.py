# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1492, 1080)
        MainWindow.setStyleSheet(u"background-color:white;")
        self.centralWidget = QWidget(MainWindow)
        self.centralWidget.setObjectName(u"centralWidget")
        self.open_file_btn = QPushButton(self.centralWidget)
        self.open_file_btn.setObjectName(u"open_file_btn")
        self.open_file_btn.setGeometry(QRect(60, 30, 186, 51))
        self.open_file_btn.setStyleSheet(u"   background-color: rgb(246, 231, 118);\n"
"    border-style: outset;\n"
"    border-width: 2px;\n"
"    border-radius: 10px;\n"
"    border-color: beige;\n"
"    font: bold 14px;\n"
"    min-width: 10em;\n"
"    padding: 6px;")
        self.spectrum_infos_label = QLabel(self.centralWidget)
        self.spectrum_infos_label.setObjectName(u"spectrum_infos_label")
        self.spectrum_infos_label.setGeometry(QRect(270, 30, 1121, 51))
        font = QFont()
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.spectrum_infos_label.setFont(font)
        self.spectrum_infos_label.setAlignment(Qt.AlignCenter)
        self.mywidget = QWidget(self.centralWidget)
        self.mywidget.setObjectName(u"mywidget")
        self.mywidget.setGeometry(QRect(0, 90, 1491, 931))
        self.verticalLayoutWidget = QWidget(self.mywidget)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 10, 1461, 861))
        self.mylayout = QVBoxLayout(self.verticalLayoutWidget)
        self.mylayout.setSpacing(6)
        self.mylayout.setContentsMargins(11, 11, 11, 11)
        self.mylayout.setObjectName(u"mylayout")
        self.mylayout.setContentsMargins(0, 0, 0, 0)
        self.file_path_label = QLabel(self.mywidget)
        self.file_path_label.setObjectName(u"file_path_label")
        self.file_path_label.setGeometry(QRect(70, 900, 1381, 21))
        self.file_path_label.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)
        self.file_path_label_title = QLabel(self.mywidget)
        self.file_path_label_title.setObjectName(u"file_path_label_title")
        self.file_path_label_title.setGeometry(QRect(10, 900, 60, 21))
        self.file_path_label_title.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)
        self.line = QFrame(self.mywidget)
        self.line.setObjectName(u"line")
        self.line.setGeometry(QRect(-23, 880, 1511, 20))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)
        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QMenuBar(MainWindow)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 1492, 24))
        MainWindow.setMenuBar(self.menuBar)
        self.mainToolBar = QToolBar(MainWindow)
        self.mainToolBar.setObjectName(u"mainToolBar")
        MainWindow.addToolBar(Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QStatusBar(MainWindow)
        self.statusBar.setObjectName(u"statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.open_file_btn.setText(QCoreApplication.translate("MainWindow", u"Open File", None))
        self.spectrum_infos_label.setText(QCoreApplication.translate("MainWindow", u"Please select a spectrum with open file button", None))
        self.file_path_label.setText("")
        self.file_path_label_title.setText(QCoreApplication.translate("MainWindow", u"File path :", None))
    # retranslateUi

