# -*- coding: utf-8 -*-

#system
import sys

import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region

# Imports maths, data et visualisation
import matplotlib.pyplot as plt

"""
À partir de notre code précédent (specviewer1.py), essayons d'organiser notre code
de sorte que l'on puisse l'utiliser plus facilement au quotidien.



    ===== Objectif du script ===
    Factorisation du code
        - Creation d'un spec1d (fct)
        - Affichage avec matplotlib (fct)
    ==========================
    

    ===== Éléments abordés =====
    Suppression des imports inutiles
    Ajout de fonctions (générer un spec1D, générer un graphe, analyse, continumm, etc..)
    Ajout d'une documentation de code
    Ajout d'un main
    ============================


"""

def prepare_spec1d(fits_file_path):

    #open file
    try:
        f = fits.open(fits_file_path)
    except FileNotFoundError:
        "File not found. Please verify path file."
    
    #get data
    header = f[0].header
    data = f[0].data
    
    # Précision des unités du flux
    flux = data * u.Jy
    
    #------ Avec le WCS object -----#
    wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                   'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                   'CRPIX1': header['CRPIX1']})
    
    # Création d'un objet Spectrum1D avec specutils
    spec = Spectrum1D(flux=flux, wcs=wcs_data)
    
    return spec, header



def prepare_spec1d_region(spec1d, region_start, region_stop):
    
    # Création d'une région spectrale autour de la raie détectée (+/- 80A)
    sr = SpectralRegion(region_start * u.AA, region_stop * u.AA)
    
    # Création d'un nouveau spectre de avec la région spectral sélectionnée
    spex = extract_region(spec1d, sr)
    spec1d_crop = Spectrum1D(flux=spex.flux,spectral_axis=spex.spectral_axis)
    
    return spec1d_crop



def show_spec1d(spec1d, header):
    
    # Init de la figure
    fig01, ax01 = plt.subplots(figsize=(16, 9))
    
    # Définition des valeurs à afficher, X et Y
    # On précise également les unités pour la longueur d'onde
    ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
    
    # Titre de l'axe X
    ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
    
    # Titre de l'axe Y
    ax01.set_ylabel('Relative Flux')
    
    # Configuration de la grille
    ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
        
    # Légende
    legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
    ax01.legend([legend_value], loc=('best'))
    
    
    # Creation d'un titre et ajout sur le graphique
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
    
    # Enregistrement en png
    #plt.savefig('spectrum.png')
    
    # Affichage
    plt.show()
    print('Plot generated.')
    
    
# __name__ est une variable automatiquement créée par Python
# Elle est disponible partout et contient le nom du script en cours (dans le cas d'un import par exemple)
# Mais si on se trouve dans le script principale --> cette variable vaudra __main__ et nous permettra d'appeler
# par exemple la méthode de lancement main()

if __name__ == "__main__":
    
    # execute only if run as a script
    # main()
    print('Scope : ' , __name__)
    
    
    filepath_spectre='data/_28tau_20131215_916.fits'

    #Création d'un spectre à partir du fichier FITS
    my_spectrum, my_header = prepare_spec1d(filepath_spectre)
    
    #Génération et affichage d'un graphe
    show_spec1d(my_spectrum, my_header)
    
    #Création d'un spectre zoomé sur Halpha
    show_spec1d((prepare_spec1d_region(my_spectrum, 6550, 6580)), my_header)

    
      