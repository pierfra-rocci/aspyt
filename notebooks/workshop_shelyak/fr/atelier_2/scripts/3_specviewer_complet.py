import sys
import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region
from specutils.fitting import fit_generic_continuum

# Imports maths, data et visualisation
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.offline import plot


"""
À partir de notre code précédent (specviewer.py), essayons d'organiser notre code
de sorte que l'on puisse l'utiliser plus facilement au quotidien.


    ===== Objectif du script ===
    Factorisation du code
        - Toutes les fonctions utiles
    ==========================
    
"""



#Préparer un spec1d à partir d'un fichier fits en paramètre
def prepare_spec1d(fits_file_path):

    #open file
    f = fits.open(fits_file_path)
    
    #get data
    header = f[0].header
    data = f[0].data
    
    # Précision des unités du flux
    flux = data * u.Jy
    
    #------ Avec le WCS object -----#
    wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                   'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                   'CRPIX1': header['CRPIX1']})
    
    # Création d'un objet Spectrum1D avec specutils
    spec = Spectrum1D(flux=flux, wcs=wcs_data)
    
    return spec, header


#Générer une région spectrale à partir d'un fichier FITS
def prepare_spec1d_region(spec1d, region_start, region_stop):
    
    # Création d'une région spectrale autour de la raie détectée (+/- 80A)
    sr = SpectralRegion(region_start * u.AA, region_stop * u.AA)
    
    # Création d'un nouveau spectre de avec la région spectral sélectionnée
    spex = extract_region(spec1d, sr)
    spec1d_crop = Spectrum1D(flux=spex.flux,spectral_axis=spex.spectral_axis)
    
    return spec1d_crop




#Afficher un spec1d avec matplotlib
def show_spec1d(spec1d, header):
    
    # Init de la figure
    fig01, ax01 = plt.subplots(figsize=(16, 9))
    
    # Définition des valeurs à afficher, X et Y
    # On précise également les unités pour la longueur d'onde
    ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
    
    # Titre de l'axe X
    ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
    
    # Titre de l'axe Y
    ax01.set_ylabel('Relative Flux')
    
    # Configuration de la grille
    ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
        
    # Légende
    legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
    ax01.legend([legend_value], loc=('best'))
    
    
    # Creation d'un titre et ajout sur le graphique
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
    
    # Enregistrement en png
    #plt.savefig('spectrum.png')
    
    # Affichage
    plt.show()
    print('Plot generated.')
    
    


#Afficher un graphe avec plotly 
def show_plotly_spec1d(spec1d, header):
    
    # Création des figures
    fig05 = go.Figure()
    fig05.add_trace(go.Scatter(x=spec1d.spectral_axis, y=spec1d.flux,
                        mode='lines',
                        name='Pleione 03-08'))
    
    
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    # Configuration  du graphique (titre, titres axes, etc.)
    fig05.update_layout(title=spectrumTitle,
                       xaxis_title=header['CTYPE1'] + ' - ' + header['CUNIT1'],
                       yaxis_title='Relative Flux')
    
    plot(fig05)



#Afficher le continuum d'un spec1d
def show_continuum(spec1d):
    
    # Préparation des données
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux
    
    # Fit du continuum (avec exclusion d'une région par exemple, ici 3600 - 4000 A)
    g1_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])
    y_continuum_fitted = g1_fit(x_spec_init)
    
    # Création du graphique
    fig02, ax02 = plt.subplots(figsize=(12,8))
    
    # Spectre
    ax02.plot(x_spec_init, y_spec_init)
    
    # Continuum
    ax02.plot(x_spec_init, y_continuum_fitted)
    
    # Affichage
    ax02.grid(True)
    plt.show()
    
    
#Normaliser un spec1d
def normalize_spec1d(spec1d):
    # Normalisation
    
    # Préparation des data
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux
    
    # Fit du continuum (avec exclusion d'une région par exemple, ici 3600 - 4000 A)
    g_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])#--> modified with balmer lines 
    #g_fit = fit_generic_continuum(spec)
    
    # Division du spectre par son continuum
    y_continuum_fitted = g_fit(x_spec_init)
    spec_normalized = spec1d / y_continuum_fitted
    
    # Affichage
    fig03, ax03 = plt.subplots(figsize=(8,5))
    ax03.plot(spec_normalized.spectral_axis, spec_normalized.flux)
    ax03.grid(True)
    plt.show()
    return spec_normalized
    
    


#--------- Main ---------#

if __name__ == "__main__":
    # execute only if run as a script

    #Récupération du chemin du fichier par un args au lancement.
    if (len( sys.argv ) > 1):
        print(sys.argv[1])
        filepath_spectre=sys.argv[1]
    else:
        print('You need to specify a file path')
        filepath_spectre='no file'
    
    
    #spec
    my_spectrum, my_header = prepare_spec1d(filepath_spectre)
    
    #general mpl plot
    #show_spec1d(my_spectrum, my_header)
    
    #generate plotly spec
    #show_plotly_spec1d(my_spectrum, my_header)
    
    #show zoom on specral region
    #show_spec1d((prepare_spec1d_region(my_spectrum, 6550, 6590)), my_header)
    
    #show continuum
    show_continuum(my_spectrum)
    
    #normalize spectrum
    normalize_spec1d(my_spectrum)

