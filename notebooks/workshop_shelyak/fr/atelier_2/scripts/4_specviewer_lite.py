"""
À partir de notre code précédent (specviewer.py), essayons d'organiser notre code
de sorte que l'on puisse l'utiliser plus facilement au quotidien.


    ========== Objectif du script ========
    Simplification script en utilisant un module
    ======================================


    ========== Éléments abordés ==========
    Mise en module des fonctions courantes
    Suppression de tous les imports
    Ajout d'une docstring dans le module
    =======================================


"""

#Imports
import sys

# Import de notre module ! :)
import betools



if __name__ == "__main__":
    # execute only if run as a script
    
    #Recupération du chemin fichier par argument
    if (len( sys.argv ) > 1):
        print(sys.argv[1])
        filepath_spectre=sys.argv[1]
    else:
        print('You need to specify a file path')
        filepath_spectre='no file'
    

    #Création d'un spec1D
    my_spectrum, my_header = betools.prepare_spec1d(filepath_spectre)

    #Affichage d'un graphe avec plotly
    #betools.show_plotly_spec1d(my_spectrum, my_header, True, 3)
    
    #Affichage du continuum
    betools.show_continuum(my_spectrum)
    
    #Version condensée
    #betools.show_continuum(betools.prepare_spec1d(filepath_spectre)[0])
   
    #normalization
    betools.normalize_spec1d(my_spectrum)

    
    

    