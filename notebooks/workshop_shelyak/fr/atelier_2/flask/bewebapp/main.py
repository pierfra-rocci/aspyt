import os
from flask import Flask, request
from werkzeug.utils import secure_filename #for secure uploads !
from flask import render_template
from plotly import io
import betools #La logique métier est ici !

#https://flask.palletsprojects.com/en/1.1.x/patterns/fileuploads/#uploading-files


# Préparation du dossier où les fichiers importés seront enregistrés
UPLOAD_FOLDER = 'files_uploads'

# Définition de l'instance Flask
app = Flask(__name__)

# Configuration du dossier Upload pour l'app Flask
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



# Route principale (index)
@app.route('/', methods=['GET'])
def get_index_page():
    return render_template('index.html')


# Route visualiseur simple 
@app.route('/simpleviewer', methods=['GET', 'POST'])
def upload_file():
    #si un fichier un envoyé -> on le récupère
    if request.method == 'POST':
        # on enregistre le retour et s'il y a un fichier on 
        file = request.files['file']
        if file :
            # Récupération du filename
            filename = secure_filename(file.filename)

            # Enregistrement du fichier dans le dossier Uploads
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            # Création du spec1D via le module betools
            sp, hd = betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

            # Création du graphe
            fig = betools.get_plotly_spec1d(sp, hd)

            #generation d'un graph html/js à partir de notre objet figure précédent 
            div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

            # Retour du template & de du graphe préparé dans la variable plot
            return render_template('simpleviewer.html', plot=div_plot)   

    #Sinon, aucun fichier n'a été envoyé, on affiche un message à la place du graphe dans la varible plot                     
    return render_template('simpleviewer.html', plot='Sélectionnez un fichier .fits')
    


# Route visualiseur multiple
@app.route("/multipleviewer", methods=['GET', 'POST'])
def multiple_files() :
    if request.method == 'POST':
        # Récupération de la liste des fichiers
        files = request.files.getlist("file")

        spec1D_list = []
        header_list = []
        # Itération sur la liste des fichiers
        # Génération d'un spec1d pour chacun d'entre eux.
        for file in files:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            spec1D_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[0])
            header_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[1])
    
        fig = betools.get_plotly_multi_spec1d(spec1D_list, header_list, True, 1.5)

        #generation d'un graphe
        div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

        return render_template('multipleviewer.html', plot=div_plot)
    return render_template('multipleviewer.html', plot='Sélectionnez plusieurs fichiers .fits')



# Route visualiseur de Timeseries 3D
@app.route("/timeseriesviewer", methods=['GET', 'POST'])
def timeseries_files() :
    if request.method == 'POST':
        files = request.files.getlist("file")

        spec1D_list = []
        header_list = []
        for file in files:
            filename = secure_filename(file.filename)

            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            print(file.filename)
            spec1D_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[0])
            header_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[1])
    
        # Appel de la fonction get_plotly_3d du module betools
        fig = betools.get_plotly_3d(spec1D_list, header_list)

        #generation d'un graph html/js à partir de notre objet figure précédent 
        div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

        return render_template('multipleviewer.html', plot=div_plot)
    return render_template('multipleviewer.html', plot='Sélectionnez plusieurs fichiers .fits')


# Route à propos
@app.route('/about')
def get_about_page():
    return render_template('about.html')


if __name__ == "__main__":
    app.run(debug=True)
    #app.run(host= '192.168.0.2')