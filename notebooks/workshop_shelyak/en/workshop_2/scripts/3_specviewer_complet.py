import sys
import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region
from specutils.fitting import fit_generic_continuum

# Imports maths, data et visualisation
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.offline import plot


"""
From our previous code (specviewer1.py), let's try to organize our code
so that we can use it more easily on a daily usage.


    ===== Script goal ===
    Factorization du code
        - Every useful functions
    ==========================
    
"""



# Prepare a spec1d from a file fits in parameter
def prepare_spec1d(fits_file_path):

    #open file
    f = fits.open(fits_file_path)
    
    #get data
    header = f[0].header
    data = f[0].data
    
    # Flux units
    flux = data * u.Jy
    
    #------ with WCS object -----#
    wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                   'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                   'CRPIX1': header['CRPIX1']})
    
    # Creating a Spectrum1D object with specutils
    spec = Spectrum1D(flux=flux, wcs=wcs_data)
    
    return spec, header


# Generate a spectral region from a FITS file
def prepare_spec1d_region(spec1d, region_start, region_stop):
    
    # Creation of a spectral region around the detected line  (+/- 80A)
    sr = SpectralRegion(region_start * u.AA, region_stop * u.AA)
    
    # Creation of a new spectrum with the selected spectral region
    spex = extract_region(spec1d, sr)
    spec1d_crop = Spectrum1D(flux=spex.flux,spectral_axis=spex.spectral_axis)
    
    return spec1d_crop




# Display a spec1d with matplotlib
def show_spec1d(spec1d, header):
    
    # Fingure init
    fig01, ax01 = plt.subplots(figsize=(16, 9))
    
    # Definition of the values to be displayed, X and Y
    # We also specify the units for the wavelength
    ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
    
    # X axis title
    ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
    
    # Y axis title
    ax01.set_ylabel('Relative Flux')
    
    # grid configuration
    ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
        
    # Legend
    legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
    ax01.legend([legend_value], loc=('best'))
    
    
    # Creating a title and adding it to the chart
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
    
    # Record in png
    #plt.savefig('spectrum.png')
    
    # Affichage
    plt.show()
    print('Plot generated.')
    
    


# Display a graph with plotly 
def show_plotly_spec1d(spec1d, header):
    
    # Creating fig
    fig05 = go.Figure()
    fig05.add_trace(go.Scatter(x=spec1d.spectral_axis, y=spec1d.flux,
                        mode='lines',
                        name='Pleione 03-08'))
    
    
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    # Chart configuration (title, axis titles, etc.)
    fig05.update_layout(title=spectrumTitle,
                       xaxis_title=header['CTYPE1'] + ' - ' + header['CUNIT1'],
                       yaxis_title='Relative Flux')
    
    plot(fig05)



# Display the continuum of a spec1d
def show_continuum(spec1d):
    
    # Data preparation
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux
    
    # Fit of the continuum (with exclusion of a region for example), ici 3600 - 4000 A)
    g1_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])
    y_continuum_fitted = g1_fit(x_spec_init)
    
    # Graphic creation
    fig02, ax02 = plt.subplots(figsize=(12,8))
    
    # Spectrum
    ax02.plot(x_spec_init, y_spec_init)
    
    # Continuum
    ax02.plot(x_spec_init, y_continuum_fitted)
    
    # Display
    ax02.grid(True)
    plt.show()
    
    
# Normalize a spec1d
def normalize_spec1d(spec1d):
    
    # Data Preparation
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux
    
    # Fit of the continuum (with exclusion of a region for example), ici 3600 - 4000 A)
    g_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])#--> modified with balmer lines 
    #g_fit = fit_generic_continuum(spec)
    
    # Division of the spectrum by its continuum
    y_continuum_fitted = g_fit(x_spec_init)
    spec_normalized = spec1d / y_continuum_fitted
    
    # Display
    fig03, ax03 = plt.subplots(figsize=(8,5))
    ax03.plot(spec_normalized.spectral_axis, spec_normalized.flux)
    ax03.grid(True)
    plt.show()
    return spec_normalized
    
    


#--------- Main ---------#

if __name__ == "__main__":
    # execute only if run as a script

    # Recovery of the file path by an args at launch.
    if (len( sys.argv ) > 1):
        print(sys.argv[1])
        filepath_spectre=sys.argv[1]
    else:
        print('You need to specify a file path')
        filepath_spectre='no file'
    
    
    #spec
    my_spectrum, my_header = prepare_spec1d(filepath_spectre)
    
    #general mpl plot
    #show_spec1d(my_spectrum, my_header)
    
    #generate plotly spec
    #show_plotly_spec1d(my_spectrum, my_header)
    
    #show zoom on specral region
    #show_spec1d((prepare_spec1d_region(my_spectrum, 6550, 6590)), my_header)
    
    #show continuum
    show_continuum(my_spectrum)
    
    #normalize spectrum
    normalize_spec1d(my_spectrum)

