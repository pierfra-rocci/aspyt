# -*- coding: utf-8 -*-

#system
import sys

import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region

# Imports maths, data et visualisation
import matplotlib.pyplot as plt

"""
From our previous code (specviewer1.py), let's try to organize our code
so that we can use it more easily on a daily usage.



    ===== Script goal ===
    Factorization of the code
        - Creation of a spec1d (fct)
        - Display with matplotlib (fct)
    ==========================
    

    ===== Elements =====
    Removal of useless imports
    Added functions (generate spec1D, generate graph, analysis, continumm, etc..)
    Added code documentation
    Add a main
    ============================


"""

def prepare_spec1d(fits_file_path):

    #open file
    try:
        f = fits.open(fits_file_path)
    except FileNotFoundError:
        "File not found. Please verify path file."
    
    #get data
    header = f[0].header
    data = f[0].data
    
    # Flux units
    flux = data * u.Jy
    
    #------ with WCS object -----#
    wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                   'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                   'CRPIX1': header['CRPIX1']})
    
    # Creating a Spectrum1D object with specutils
    spec = Spectrum1D(flux=flux, wcs=wcs_data)
    
    return spec, header



def prepare_spec1d_region(spec1d, region_start, region_stop):
    
    # Creation of a spectral region around the detected line (+/- 80A)
    sr = SpectralRegion(region_start * u.AA, region_stop * u.AA)
    
    # Creation of a new spectrum with the selected spectral region
    spex = extract_region(spec1d, sr)
    spec1d_crop = Spectrum1D(flux=spex.flux,spectral_axis=spex.spectral_axis)
    
    return spec1d_crop



def show_spec1d(spec1d, header):
    
    # Figure init
    fig01, ax01 = plt.subplots(figsize=(16, 9))
    
    # Definition of the values to be displayed, X and Y
    # We also specify the units for the wavelength
    ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
    
    # X axis title
    ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
    
    # Y axis title
    ax01.set_ylabel('Relative Flux')
    
    # Grid configuration
    ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
        
    # Legend
    legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
    ax01.legend([legend_value], loc=('best'))
    
    
    # Creating a title and adding it to the chart
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
    
    # Record in png
    #plt.savefig('spectrum.png')
    
    # Display
    plt.show()
    print('Plot generated.')
    
    
# __name__ is a variable automatically created by Python
# It is available everywhere and contains the name of the current script (in the case of an import for example)
# But if we are in the main script --> this variable will be __main__ and will allow us to call
# for example the main() method

if __name__ == "__main__":
    
    # execute only if run as a script
    # main()
    print('Scope : ' , __name__)
    
    
    filepath_spectre='data/_28tau_20131215_916.fits'

    # Creating a spectrum from the FITS file
    my_spectrum, my_header = prepare_spec1d(filepath_spectre)
    
    # Generation and display of a graph
    show_spec1d(my_spectrum, my_header)
    
    # Creating a zoomed spectrum on Halpha
    show_spec1d((prepare_spec1d_region(my_spectrum, 6550, 6580)), my_header)

    
      