"""
From our previous code (specviewer1.py), let's try to organize our code
so that we can use it more easily on a daily usage.


    ========== Script goal ========
    Script simplification using a module
    ======================================


    ========== Elements ==========
    Putting the current functions into a module
    Delete all unused imports
    Add a docstring in the module
    =======================================


"""

#Imports
import sys

# Import de notre module ! :)
import betools



if __name__ == "__main__":
    # execute only if run as a script
    
    #Recupération du chemin fichier par argument
    if (len( sys.argv ) > 1):
        print(sys.argv[1])
        filepath_spectre=sys.argv[1]
    else:
        print('You need to specify a file path')
        filepath_spectre='no file'
    

    #Création d'un spec1D
    my_spectrum, my_header = betools.prepare_spec1d(filepath_spectre)

    #Affichage d'un graphe avec plotly
    #betools.show_plotly_spec1d(my_spectrum, my_header, True, 3)
    
    #Affichage du continuum
    betools.show_continuum(my_spectrum)
    
    #Version condensée
    #betools.show_continuum(betools.prepare_spec1d(filepath_spectre)[0])
   
    #normalization
    betools.normalize_spec1d(my_spectrum)

    
    

    