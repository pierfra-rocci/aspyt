from flask import Flask
from flask import render_template
from plotly import io

import betools

"""
Creation of a simple Flask application

    ===== Script goal ===
    Basic principle of a Flask application
        - Preparing and displaying a spectrum
        - Display with matplotlib (fct)
    ==========================
    

    ===== Items =====
    Route
    Template
    Intro Bootstrap : https://getbootstrap.com/docs/5.0/examples/
    URL static : {{ url_for('static', filename='sticky-footer.css') }}
    ============================
"""



# Flask instance preparation
app = Flask(__name__)


# Principal route
@app.route('/')
def index():
    return '<b>Index Page</b>'


# hello route
@app.route('/hello')
def hello():
    name_to_show ='Matthieu'
    return render_template('design.html', username = name_to_show)

# spectrum route
@app.route('/spectrum') #,methods=['GET', 'POST'])
def spectrum():

    # Spec preparation
    spec = betools.prepare_spec1d('data/28tau_20131215_916.fits')
    # Get fig
    fig_plotly = betools.get_plotly_spec1d(spec[0], spec[1], False)
    
    
    #-------- Generate a html/js graph from our fig object with the io.to_html plotly method ------#
    div_plot = io.to_html(fig_plotly, full_html=False, include_plotlyjs=True)


    # Return a spectra.html template and the div_plot element
    return render_template('spectra.html', plot = div_plot)



if __name__ == "__main__":
   app.run(debug=True) # Start app
   #app.run(host= '192.168.0.2')
 
    