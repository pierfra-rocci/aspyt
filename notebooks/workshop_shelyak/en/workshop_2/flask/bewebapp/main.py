import os
from flask import Flask, request
from werkzeug.utils import secure_filename #for secure uploads !
from flask import render_template
from plotly import io
import betools #La logique métier est ici !

#https://flask.palletsprojects.com/en/1.1.x/patterns/fileuploads/#uploading-files


# Preparation of the folder where the imported files will be saved
UPLOAD_FOLDER = 'files_uploads'

# Definition of the Flask instance
app = Flask(__name__)

# Upload folder configuration for the Flask app
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



# Principal route (index)
@app.route('/', methods=['GET'])
def get_index_page():
    return render_template('index.html')


# Simple visualiser Route
@app.route('/simpleviewer', methods=['GET', 'POST'])
def upload_file():
    # if a file is sent -> it is retrieved
    if request.method == 'POST':
        # we record the return and if there is a file we check it
        file = request.files['file']
        if file :
            # Recovering the filename
            filename = secure_filename(file.filename)

            # Save the file in the Uploads folder
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            # Creation of the spec1D via the betools module
            sp, hd = betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

            # Creation of the graph
            fig = betools.get_plotly_spec1d(sp, hd)

            # generation of a html/js graph from our previous figure object 
            div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

            # Return of the template & of the prepared graph in the plot variable
            return render_template('simpleviewer.html', plot=div_plot)   

    # Otherwise, no file has been sent, a message is displayed instead of the graph in the plot varible                    
    return render_template('simpleviewer.html', plot='Sélectionnez un fichier .fits')
    


# Multiple visualiser Route
@app.route("/multipleviewer", methods=['GET', 'POST'])
def multiple_files() :
    if request.method == 'POST':
        # Recovery of the list of files
        files = request.files.getlist("file")

        spec1D_list = []
        header_list = []
        # Iteration on the list of files
        # Generation of a spec1d for each of them.
        for file in files:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            spec1D_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[0])
            header_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[1])
    
        fig = betools.get_plotly_multi_spec1d(spec1D_list, header_list, True, 1.5)

        # generation of a graph
        div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

        return render_template('multipleviewer.html', plot=div_plot)
    return render_template('multipleviewer.html', plot='Sélectionnez plusieurs fichiers .fits')



# 3D Timeseries visualiser Route
@app.route("/timeseriesviewer", methods=['GET', 'POST'])
def timeseries_files() :
    if request.method == 'POST':
        files = request.files.getlist("file")

        spec1D_list = []
        header_list = []
        for file in files:
            filename = secure_filename(file.filename)

            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            print(file.filename)
            spec1D_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[0])
            header_list.append(betools.prepare_spec1d(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))[1])
    
        # Calling the get_plotly_3d function of the betools module
        fig = betools.get_plotly_3d(spec1D_list, header_list)

        # generation of a html/js graph from our previous figure object 
        div_plot = io.to_html(fig, full_html=False, include_plotlyjs=True)

        return render_template('multipleviewer.html', plot=div_plot)
    return render_template('multipleviewer.html', plot='Sélectionnez plusieurs fichiers .fits')


# about Route
@app.route('/about')
def get_about_page():
    return render_template('about.html')


if __name__ == "__main__":
    app.run(debug=True)
    #app.run(host= '192.168.0.2')