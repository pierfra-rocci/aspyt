# -*- coding: utf-8 -*-
import sys
import astropy.units as u
from astropy.io import fits
import astropy.wcs as fitswcs #wcs
from specutils import Spectrum1D, SpectralRegion #spectrum1D (specutils)
from specutils.manipulation import extract_region
from astropy.modeling import models, fitting
from specutils.fitting import fit_generic_continuum

# Imports maths, data et visualisation
import matplotlib.pyplot as plt
from matplotlib import colors
import plotly.graph_objects as go
from plotly.offline import download_plotlyjs, init_notebook_mode,  plot


#print scope
#print(__name__)

"""

@authors : M. Le Lain

This module containes methods for 
create, manipulate, display a spectrum1d quickly with specutils,
matplotlib, and plotly

"""



def prepare_spec1d(fits_file_path):
    """
    Prepare a spec1D from fits file path.
    Flux in jansky
    Spectral axis in angstromwith FistWCS preparation

    Parameters
    ----------
    fits_file_path : Str
        Full path of fits file.

    Returns
    -------
    spec : spec1d
        Spec1D Spectrum.
    header : HDU List
        Header of the fits file.

    """

    #open file
    f = fits.open(fits_file_path)
    
    #get data
    header = f[0].header
    data = f[0].data
    
    # Flux units
    flux = data * u.Jy
    
    #------ With WCS object -----#
    wcs_data = fitswcs.WCS(header={'CDELT1': header['CDELT1'], 'CRVAL1': header['CRVAL1'],
                                   'CUNIT1': header['CUNIT1'], 'CTYPE1': header['CTYPE1'],
                                   'CRPIX1': header['CRPIX1']})
    
    # Create a spectrum1D object with specutils
    spec = Spectrum1D(flux=flux, wcs=wcs_data)
    
    return spec, header





def show_continuum(spec1d):
    """
    Generate a plot with continuum of a spec1D  with matplotlib  

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum.

    Returns
    -------
    None.

    """
    
    
    # Data preparation
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux
    
    # Continuum fit (with spectral region exlude, here 3600 - 4000 A)
    g1_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])
    y_continuum_fitted = g1_fit(x_spec_init)    
    
    # Grapphic creation
    fig02, ax02 = plt.subplots(figsize=(12,8))
    
    # Spectrum
    ax02.plot(x_spec_init, y_spec_init)
    
    # Continuum
    ax02.plot(x_spec_init, y_continuum_fitted)
    
    # Show
    ax02.grid(True)
    plt.show()
    
    
    
    
def normalize_spec1d(spec1d):
    """
    Normalize a spec1d

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to normalize.

    Returns
    -------
    spec_normalized : spec1d
        a spec1d normalized.

    """
    # Normalisation
    
    # Data preparation
    x_spec_init = spec1d.spectral_axis
    y_spec_init = spec1d.flux # --> can be delete !
    
    # Continuum fit (with spectral region exclusion, here 3600 - 4000 A)
    g_fit = fit_generic_continuum(spec1d, exclude_regions=[SpectralRegion(3600 * u.AA, 4000 * u.AA)])#--> modified with balmer lines 
    #g_fit = fit_generic_continuum(spec)
    
    # Divide the spectrum with his continuum
    y_continuum_fitted = g_fit(x_spec_init)
    spec_normalized = spec1d / y_continuum_fitted
    
    # Show
    fig03, ax03 = plt.subplots(figsize=(8,5))
    ax03.plot(spec_normalized.spectral_axis, spec_normalized.flux)
    ax03.grid(True)
    plt.show()
    return spec_normalized




def prepare_spec1d_region(spec1d, region_start, region_stop):
    """
    Prepare a new spec1d for a spectral region (start, stop)

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum.
    region_start : float or integer
        region start value.
    region_stop : float or interger
        region stop vlaue.

    Returns
    -------
    spec1d_crop : spec1d
        a spec1d of the selected region.

    """
    
    # Create a spectral region around the line (+/- 80A)
    sr = SpectralRegion(region_start * u.AA, region_stop * u.AA)
    
    # Create a new spectrum from the spectral region selected
    spex = extract_region(spec1d, sr)
    spec1d_crop = Spectrum1D(flux=spex.flux,spectral_axis=spex.spectral_axis)
    
    return spec1d_crop



def show_spec1d(spec1d, header):
    """
    Generate a show a spectrum with Matplotlib

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to plot.
    header : HDU
        The header of the spectrum

    Returns
    -------
    None.

    """
    
    # Init de la figure
    fig01, ax01 = plt.subplots(figsize=(16, 9))
    
    # Prepare spec1d with X and Y values
    # We precise alsi the units
    ax01.plot(spec1d.spectral_axis * u.AA, spec1d.flux)
    
    #  X axis title
    ax01.set_xlabel(header['CTYPE1'] + ' - ' + header['CUNIT1'])
    
    # Y axis title
    ax01.set_ylabel('Relative Flux')
    
    # Grid configuration
    ax01.grid(color='grey', alpha=0.8, linestyle='-.', linewidth=0.2, axis='both') 
        
    # Legend
    legend_value = header['OBJNAME'] + ' - ' + header['DATE-OBS']
    ax01.legend([legend_value], loc=('best'))
    
    
    # Create a title and add it on the graph
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    ax01.set_title(spectrumTitle, loc='center', fontsize=14, fontweight=0.5)
    
    # Record in png
    #plt.savefig('spectrum.png')
    
    # Show plot
    plt.show()
    print('Plot generated.')



    
def show_plotly_spec1d(spec1d, header, second_axis=False, second_axis_ratio=1.2):
    """
    Generate and show a spectrum plot with Plotly.
    Can show second axis with ratio or not (watch parameters)

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to plot.
    header : HDU
        the header of the spectrum.
    second_axis : boolean
        show or not a second axis.
    second_axis_ratio : float
        ratio multiple of the second axis.

    Returns
    -------
    None.

    """
    
    # Figure creation
    fig05 = go.Figure()
    fig05.add_trace(go.Scatter(x=spec1d.spectral_axis, y=spec1d.flux,
                        mode='lines',
                        #name='Pleione 03-08'))
                        name=header['OBJNAME']))
    
    #Add a second graph
    fig05.add_trace(go.Scatter(x=spec1d.spectral_axis, y=spec1d.flux*second_axis_ratio,
                        mode='lines+markers',
                        #name='Pleione 04-08'))
                        name=header['OBJNAME']))
    
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    # Configuation  du graphique (titre, titres axes, etc.)
    fig05.update_layout(title=spectrumTitle,
                       xaxis_title=header['CTYPE1'] + ' - ' + header['CUNIT1'],
                       yaxis_title='Relative Flux')
    
    plot(fig05)




    
def get_plotly_spec1d(spec1d, header, second_axis=False, second_axis_ratio=1.2):
    """
    Generate and return a plotly figure of a spectrum

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to plot.
    header : HDU
        the header of the spectrum.
    second_axis : boolean
        show or not a second axis.
    second_axis_ratio : float
        ratio multiple of the second axis.

    Returns
    -------
    plotly_fig : figure
        a plotly figure of the spec1d

    """
    
    # figures creation
    plotly_fig = go.Figure()
    plotly_fig.add_trace(go.Scatter(x=spec1d.spectral_axis, y=spec1d.flux,
                        mode='lines',
                        #name='Pleione 03-08'))
                        name=header['OBJNAME']))
        
    spectrumTitle = header['OBJNAME'] + ' - ' + header['DATE-OBS'] + ' - ' + header['EXPTIME2']+ ' - ' + str(header['DETNAM'] + ' - ' + header['OBSERVER'])
    # Graphic configuration (title, axes titles, etc.)
    plotly_fig.update_layout(title=spectrumTitle,
                       xaxis_title=header['CTYPE1'] + ' - ' + header['CUNIT1'],
                       yaxis_title='Relative Flux')
    
    return plotly_fig

    



    
def get_plotly_multi_spec1d(spec1dlist, headerlist, spec_shift=False, shift_ratio=1):
    """
    Generate and show a spectrum plot with Plotly

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to plot.
    header : HDU
        the header of the spectrum.
    second_axis : boolean
        show or not a second axis.
    second_axis_ratio : float
        ratio multiple of the second axis.

    Returns
    -------
    plotly_fig : figure
        all the the spec1d on a plotly figure

    """
    plotly_fig = go.Figure()

    for index, spec_in_progress in enumerate(spec1dlist):
        #print(index, spec_in_progress)
        #print(headerlist[index])
        if(spec_shift):
            plotly_fig.add_trace(go.Scatter(x=spec_in_progress.spectral_axis, y=spec_in_progress.flux * (index + shift_ratio),
                        mode='lines',
                        name=headerlist[index]['OBJNAME']))
        else:
            plotly_fig.add_trace(go.Scatter(x=spec_in_progress.spectral_axis, y=spec_in_progress.flux,
                        mode='lines',
                        name=headerlist[index]['OBJNAME']))
  

    plotly_fig.update_layout(title='Affichage des spectres sélectionnés',
                       xaxis_title='Angstroms',
                       yaxis_title='Relative Flux')
    
    return plotly_fig





def get_plotly_3d(spec_list, header_list):
    """
    Generate and 3D surface plot for timeseries with plotly

    Parameters
    ----------
    spec1d : spec1d
        a spec1d spectrum to plot.
    header : HDU
        the header of the spectrum.

    Returns
    -------
    plotly_fig : figure
        all the the spec1d on a 3D Surface plotly

    """

    #init x,y,z arrays
    x_array = []
    z_array = []
    y_array = []


    #parse spec list and create a spec for each item
    for index, spec in enumerate(spec_list):
    #y_pos = listspec.index(item)
   
        #add values to x,y,z array
        x_array.append(spec.spectral_axis.value)
        z_array.append(spec.flux.value)
        y_array.append(header_list[index]['DATE-OBS'])

        #create figure
        #color list here : https://plotly.com/python/builtin-colorscales/
    fig = go.Figure(data=[go.Surface(z=z_array, y=y_array, x=x_array, colorscale='Viridis')]) 

    #set legend, size,...
    fig.update_layout(title='Timeseries 3D Evolution plot.', autosize=True, height=800,

                    scene = {
                            "xaxis": {"nticks": 10, "title" : "Wavelength (Å)"},
                            "yaxis": {"nticks": 10, "title" : "Date"},
                            "zaxis": {"nticks": 5, "title" : "Flux"},
                            'camera_eye': {"x": 0, "y": -1, "z": 0.5},
                            "aspectratio": {"x": 1, "y": 1, "z":0.3}
                        }
                    )

    #show contour on top
    #fig.update_traces(contours_z=dict(show=True, usecolormap=True,
    #                                  highlightcolor="limegreen", project_z=True))

    #show plot
    return fig









#================ Main Script if not used as a module ======================#


if __name__ == "__main__":
    
    
    #Get the file path from the args
    if (len( sys.argv ) > 1):
        print(sys.argv[1])
        filepath_spectre=sys.argv[1]
    else:
        print('You need to specify a file path')
        filepath_spectre='no file'

    # Create a spec1D 
    my_spectrum, my_header = prepare_spec1d(filepath_spectre)

    #Show a plot with plotly
    show_plotly_spec1d(my_spectrum, my_header, True, 1.5)
    
    # Show continuum
    show_continuum(my_spectrum)
    
    #Condensed version
    show_continuum(prepare_spec1d(filepath_spectre)[0])
   
    #normalization
    normalize_spec1d(my_spectrum)

    


