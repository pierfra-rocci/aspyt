import sys

#Astropy imports
import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt
from astropy.io import fits
from specutils import Spectrum1D

#Bokeh imports
from bokeh.io import output_notebook, show, output_file
from bokeh.plotting import figure
from bokeh.models.tools import HoverTool
from bokeh.models import ColumnDataSource, NumeralTickFormatter
from bokeh.models import Span, LabelSet, Label

__author__ = "chronosastro@gmail.com"
__license__ = "GPLv3"
__version__ = "1.2.1"


class SpecBok():

    """
    Class: Specbok

    SpecBok script parses astronomical Fits files for initiate a Spectrum Graph with 2 options :
        * Generate a Matplotlib Graph
        * Generate a web interactive graph file in HTML / JavaScript.

    Based on
        SpecUtils
        Matplotlib
        Bokeh

    Functions:
        - generateGraph(self): ior generate a Matplotlib Graph
        - generateWebOutput(self): For generate a web interactive graph in HTML/JS 

    Usage
        - For generate a Matplotlib Graph
        $ python3 specbok.py pathToFitsFile

        - For generate a web interactive graph in HTML/JS
        $ python3 specbok.py pathToFitsFile web

        - For generate a web interactive graph in HTML/JS and showing Balmer Lines
        $ python3 specbok.py pathToFitsFile web showBalmer

    """

    #Set Balmer Lines values
    balmerLines = {
		"Hα" : 6562.82,
		"Hβ" : 4861.33,
		"Hɣ" : 4340.47,
		"Hδ" : 4101.74,
        "Hε" : 3970.07,
        "Hζ" : 3889.04,
        "Hη" : 3835.38
	}

    spec1d = ""
    spectrumTitle = ""
    header = ""
    showBalmer = False

    

    def __init__(self):

        print(f"# --- Init Spectrum with file {sys.argv[1]} --- #")

        #Get file and print header informations
        filePath = sys.argv[1]
        print(f'File loading : {filePath}')
        hdul = fits.open(filePath)
        specdata = hdul[0].data
        self.header = hdul[0].header
        print(repr(self.header))

        # Get first pixel reference
        xRef = self.header['CRPIX1'] - 1

        #Get length of data axis1
        xlength = self.header['NAXIS1']

        #Get Wavelength pixel step (Angstrom) and convert
        lambdaStep = self.header['CDELT1']
        lambda1 = self.header['CRVAL1'] - lambdaStep * xRef
        lambda2 = self.header['CRVAL1'] + lambdaStep * (xlength - xRef)

        #Format dataset into astropy quantities
        flux= specdata * u.Jy
        wavelength = np.arange(lambda1, lambda2, lambdaStep) * u.AA

        # Spectrum construction
        self.spec1d = Spectrum1D(spectral_axis=wavelength, flux=flux)

        self.spectrumTitle = self.header['OBJNAME'] + ' - ' + self.header['DATE-OBS'] + ' - ' + str(self.header['EXPTIME']) +  's. - ' + self.header['BSS_INST'] + ' - ' + self.header['OBSERVER']

        print(f'Spectrum title : {self.spectrumTitle}')


    def generateGraph(self):
        """
            This method generate a Matplotlib Spectrum
        """
        
        #Generate Graph
        fig, ax = plt.subplots(figsize=(11,6))
        ax.plot(self.spec1d.spectral_axis, self.spec1d.flux, label=self.header['OBJNAME'], color="orange", alpha=1, lw=0.8)

        #Add Graph title
        ax.set_title(self.spectrumTitle, loc='center', fontsize=10, fontweight=0, color='black')

        #Add X axis label
        ax.set_xlabel(self.header['CTYPE1'] + ' - ' + self.header['CUNIT1'], fontdict=None, labelpad=None)

        #Add Y axis label
        ax.set_ylabel('Relative Flux', fontdict=None, labelpad=None)

        #Add grid and legend and show graph window
        ax.grid(color='grey', alpha=0.4, linestyle='-', linewidth=0.5, axis='both')
        ax.legend()
        plt.show()
        #TODO Add balmer lines
    

    def generateWebOutput(self, balmer):
        """
            This methode generate a web interactive graph file in HTML / JavaScript.
        """

        print("# --- Generate Web Output --- #")

        #set balmerLines boolean
        self.showBalmer = balmer
        print("Show Balmer Lines : " + str(self.showBalmer))

        #Graph configuration
        p = figure(title=self.spectrumTitle, title_location='above', sizing_mode="scale_width", plot_width=800, plot_height=500)

        #Add Y Grid line - Set color to none
        p.ygrid.grid_line_color = None

        #Add X axis label
        p.xaxis.axis_label = self.header['CTYPE1'] + ' - ' + self.header['CUNIT1']

        #Add Y axis Label
        p.yaxis.axis_label = 'Relative Flux'

        #Set Title configuration
        p.title.text_color = "black"
        p.title.text_font = "times"
        p.title.text_font_style = "italic"

        #Set background configuration
        p.background_fill_color = "white"
        p.background_fill_alpha = 0.5

        #Change X axis orientation label
        #p.xaxis.major_label_orientation = 1.2


        #------------Hover configuration -----------------#

        TOOLTIPS = [
            ("Wavelentgh", "($x{0,0.00})"),
            ("Intensity", "($y{0,0.00})"),
        ]

        # Add the HoverTool to the figure for showing spectrum values
        p.add_tools(HoverTool(tooltips=TOOLTIPS, mode='vline'))


        #----------- Print Balmer lines on graph -------------#

        print(self.balmerLines)

        if(self.showBalmer):
            for line, val in self.balmerLines.items():
                p.add_layout(Span(location=val,dimension='height',
                                    line_color='red',line_dash='dashed',
                                    line_width=1))

                p.add_layout(Label(x=val-2, y=70, x_units='data', y_units='screen',
                                    text=line + ' - ' + str(val), render_mode='canvas',
                                    angle=90, angle_units='deg',
                                    text_font_size='8pt'))

        #--------------------- End Balmer Lines -------------------#


        #Set spectrum to figure
        p.line(self.spec1d.spectral_axis, self.spec1d.flux.value, legend_label = self.header['OBJNAME'] + ' ' + self.header['DATE-OBS'])

        #Set legen configuration (position and show/hide)
        p.legend.location = "top_left"
        p.legend.click_policy="hide"

        # Output to HTML file
        output_file(self.header['OBJNAME']+'.html', title=self.header['OBJNAME'])

        #Print figure
        show(p) 

        print(f"# --- Web file generate without error at {self.header['OBJNAME']}.html --- #")


#Todo - modifiy with argparse
if __name__ == '__main__':

    specBok = SpecBok()

    #If no img path found -> exit
    if(len(sys.argv) < 2):
        print(f'Error - Path to Spectrum Fits file missing.')
        sys.exit(0)

    #If only img path found -> Generate Matplotlib Graph
    elif(len(sys.argv) == 2):
        specBok.generateGraph()

    #If img path and web args found -> Generate Bokeh Graph 
    elif(len(sys.argv) == 3):
        if(sys.argv[2] == "web"):
            specBok.generateWebOutput(balmer=False)
        else:
            print('Error - Web argument not found - Type "web" in command for generate HTML Graphic')
            #If img path and web args found -> Generate Bokeh Graph 

    #If img path, web args and balmer args found -> Check balmer args for showing or not Balmer Lines
    elif(len(sys.argv) == 4):
        if(sys.argv[2] == "web"):
            if(sys.argv[3] == "noBalmer"):
                specBok.generateWebOutput(balmer=False)
            elif(sys.argv[3] == 'showBalmer'):
                specBok.generateWebOutput(balmer=True)
            else:
                print("Error - Argument found is not correct - Type showBalmer or noBalmer for showing or not Balmer Lines")
        else:
            print('Error - Web argument not found - Type "web" in command for generate HTML Graphic')

    else:
        print("Error - Too many args")